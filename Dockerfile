ARG DISTRO=melodic

FROM ros:$DISTRO

ARG RASPICAM_V=0.3.0

MAINTAINER Kawin Nikomboriak concavemail@gmail.com

RUN bash -c \
        'mkdir -p /catkin_ws/src/ \
        && cd catkin_ws/src \
        && git clone https://github.com/UbiquityRobotics/raspicam_node.git \
        && cd raspicam_node \
        && git checkout "tags/$RASPICAM_V" \
        && echo \
        "yaml https://raw.githubusercontent.com/UbiquityRobotics/rosdep/master/raspberry-pi.yaml" \
        >> /etc/ros/rosdep/sources.list.d/30-ubiquity.list \
        && apt-get update \
        && apt-get install -y software-properties-common \
        && add-apt-repository ppa:ubuntu-raspi2/ppa \
        && apt-get update \
        && apt-get upgrade  -y \
        && rosdep update \
        && rosdep install -iy --from-paths /catkin_ws/src \
        && cd /catkin_ws \
        && source /opt/ros/$ROS_DISTRO/setup.bash \
        && catkin_make'

COPY ./entrypoint.sh /

ENTRYPOINT ["/entrypoint.sh"]
CMD ["bash", "-ic", "roslaunch raspicam_node camerav2_1280x960.launch"]
