#+title: ROSpberry Pi

This is a docker image which features [[https://github.com/UbiquityRobotics/raspicam_node][raspicam_node]] in a ros container.
It is available on DockerHub as [[https://hub.docker.com/r/concavegit/rospberry-pi/][concavegit/rospberry-pi]].

* Usage
- camera module v2 :: =docker run -it --rm --privileged --net=host concavegit/rospberry-pi=
- otherwise :: =docker run -it --rm --privileged --net=host concavegit/rospberry-pi bash= followed by the ROS commands of your choosing, such as =roscore=.
